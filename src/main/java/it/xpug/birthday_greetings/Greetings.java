package it.xpug.birthday_greetings;

public class Greetings {
	private String subject;
	private String message;
	private Employee employee;
	public Greetings(Employee employee){
		this.employee = employee;
	}
	
	public Greetings birthday() {
		this.subject = "Happy Birthday!";
		this.message = "Happy Birthday, dear %NAME%!".replace("%NAME%",
				employee.getFirstName());

		return this;
	}

	public String getMail() {
		return employee.getEmail();
	}
	
	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

}
